<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (Auth::check()) {
    	return view('home');
    }else{
			\Session::flush();
			//\Session::regenerate();
			Auth::logout(); 
			
			return redirect('/login');	
	}
});



Route::any('logout',function(){
	\Session::flush();
	//\Session::regenerate();
	Auth::logout();
	return redirect('/login');
})->name('logout');

Route::get('/home',function(){
	if (Auth::check()) {
	  	if (\Session::has('status')) {
			return view('home');
     	}else{
     		\Session::flush();
			//\Session::regenerate();
			Auth::logout();
     		return redirect('/login');	
     	}
     	
	}else{
		\Session::flush();
		//\Session::regenerate();
		Auth::logout();
		return redirect('/login');	
	}
});

Auth::routes(); 

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/clientes', [App\Http\Controllers\Clientes\ClientesController::class, 'index'])->name('clientes');
Route::get('/clientes/create', [App\Http\Controllers\Clientes\ClientesController::class, 'create']);
Route::post('/clientes/store', [App\Http\Controllers\Clientes\ClientesController::class, 'store']);  
Route::post('/clientes/update/{id}', [App\Http\Controllers\Clientes\ClientesController::class, 'update']); 
Route::get('/clientes/list', [App\Http\Controllers\Clientes\ClientesController::class, 'list']);

Route::get('/clientes/create', [App\Http\Controllers\Clientes\ClientesController::class, 'create']);
Route::get('/clientes/edit/{id}', [App\Http\Controllers\Clientes\ClientesController::class, 'edit']);
Route::get('/clientes/delete/{id}', [App\Http\Controllers\Clientes\ClientesController::class, 'delete']);

Route::get('/usuarios', [App\Http\Controllers\Auth\UsuariosController::class, 'index'])->name('index');
Route::get('/usuarios/create', [App\Http\Controllers\Auth\UsuariosController::class, 'create']);
Route::get('/usuarios/edit/{id}', [App\Http\Controllers\Auth\UsuariosController::class, 'edit']);
Route::post('/usuarios/update/{id}', [App\Http\Controllers\Auth\UsuariosController::class, 'update']); 
Route::get('/usuarios/delete/{id}', [App\Http\Controllers\Auth\UsuariosController::class, 'delete']);
