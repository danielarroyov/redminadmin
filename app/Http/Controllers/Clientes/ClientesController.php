<?php

namespace App\Http\Controllers\Clientes;

use Illuminate\Http\Request; 
use Validator;
use App\Http\Controllers\Controller; 
use App\Models\Clientes;

use App\User; 

class ClientesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $clientes =  Clientes::get();
        //dd($clientes);
        return view('clientes.index',compact('clientes'));
    } 

    public function create()
    { 
        return view('clientes.create');
    } 
    public function list()
    { 
        $clientes =  Clientes::get();
        //dd($clientes);
        return view('clientes.list_client_edit_delete',compact('clientes'));
    } 

    public function  edit($Id)
    { 
        
        //$clientes = Clientes::where('nit',$nit)->get();
        $clientes = Clientes::find($Id);
        //dd($clientes);
        return view('clientes.edit',compact('clientes'));
    } 

    public function store(Request $request)
    { 

        $msg = array();
        $messages = [  
            'first_name.required' => 'first_name es requerido... Obligatorio',
            'last_name.required' => 'last_name es requerido... Obligatorio',
            'nit.required' => 'nit es requerido... Obligatorio',
            'direccion.required' => 'direccion es requerido... Obligatorio',
            'city.required' => 'city es requerido... Obligatorio',
            'department.required' => 'department es requerido... Obligatorio'
            ];
            //'identificacion' => 'required|unique:medida|max:20',
            $validator = Validator::make($request->all(), [ 
                'first_name' => 'required',
                'last_name' => 'required',
                'nit' => 'required',
                'direccion' => 'required',
                'city' => 'required',
                'department' => 'required'

            ],$messages);

            if ($validator->fails()) { 
                $failed = $validator->failed(); 
                //dd($failed);
                $msg[] = "Error Valide los datos ingresados";
                //dd($validator);
                return view('clientes.store')
                            ->withErrors($validator)
                            ->with("msg",$msg)
                            ->with("guardar",false)
                            ->with("failed",$failed);
            } 
        //$sqlrest = Clientes::get();
        //dd($sqlrest);
        try {
                $cli =  new Clientes();
                //dd($cli);
                $cli_temp = new Clientes();
                $cli_temp = $cli_temp->find($request->input('nit'));
                    if(!empty($cli_temp)){
                        $cli = $cli_temp;
                    }
                //dd($cli);       
                $cli->nit              = $request->input('nit'); 
                $cli->nombre           = $request->input('first_name');
                $cli->apellido         = $request->input('last_name');
                $cli->direccion        = $request->input('direccion');
                $cli->cod_ciudad       = $request->input('city');  
                $cli->cod_departamento = $request->input('department'); 
                //dd($cli);
                if($cli->save()==false){
                    //echo $this->deshabilitado;
                    \DB::rollback();
                    $msg[] = "Error al guardar el cliente";
                        return view('clientes.store')
                                    ->with("msg",$msg)
                                    ->with("guardar",false);
                }
                return redirect('/clientes');  
        } catch (Exception $e) {
            $msg[] = "Error guardando Clientes";
                        return view('clientes.store')
                                    ->with("msg",$msg)
                                    ->with("errors",$e)
                                    ->with("guardar",false);
            
        }


        return view('clientes.store');
    }    

    public function update($id ,Request $request)
    { 

        $msg = array();
        $messages = [  
            'first_name.required' => 'first_name es requerido... Obligatorio',
            'last_name.required' => 'last_name es requerido... Obligatorio',
            'nit.required' => 'nit es requerido... Obligatorio',
            'direccion.required' => 'direccion es requerido... Obligatorio',
            'city.required' => 'city es requerido... Obligatorio',
            'department.required' => 'department es requerido... Obligatorio'
            ];
            //'identificacion' => 'required|unique:medida|max:20',
            $validator = Validator::make($request->all(), [ 
                'first_name' => 'required',
                'last_name' => 'required',
                'nit' => 'required',
                'direccion' => 'required',
                'city' => 'required',
                'department' => 'required'

            ],$messages);

            if ($validator->fails()) { 
                $failed = $validator->failed(); 
                //dd($failed);
                $msg[] = "Error Valide los datos ingresados";
                //dd($validator);
                return view('clientes.store')
                            ->withErrors($validator)
                            ->with("msg",$msg)
                            ->with("guardar",false)
                            ->with("failed",$failed);
            } 
        //$sqlrest = Clientes::get();
        //dd($sqlrest);
        try {
                $updateRows = Clientes::whereId($request->input('id'))
                        ->update([
                                    'nit'              => $request->input('nit'), 
                                    'nombre'           => $request->input('first_name'),
                                    'apellido'         => $request->input('last_name'),
                                    'direccion'        => $request->input('direccion'),
                                    'cod_ciudad'       => $request->input('city'),  
                                    'cod_departamento' => $request->input('department')
                                ]); 
                $msg[] = "guardado satisfactoriamente";
                        return view('clientes.store')
                                    ->with("msg",$msg) 
                                    ->with("guardar",true)
                                    ->with("cli",true);
        } catch (Exception $e) {
            $msg[] = "Error guardando Clientes";
                        return view('clientes.store')
                                    ->with("msg",$msg)
                                    ->with("errors",$e)
                                    ->with("guardar",false);
            
        }


        return view('clientes.store');
    } 
    public function delete($id)
    {
        $delete_cli = Clientes::find($id);
        //dd($delete_cli);
        $deletedRows = Clientes::where('id', $id)->delete();
         
        return redirect('/clientes/list');
    }
}
