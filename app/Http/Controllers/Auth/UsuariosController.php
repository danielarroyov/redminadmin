<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsuariosController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $auRaw = User::get();
        //dd($auRaw);
        return view('auth.index',compact('auRaw'));
    }

    public function create($value='')
    {
        return view('auth.register');
    }

    public function  edit($id)
    { 
        
        //$clientes = Clientes::where('nit',$nit)->get();
        $auRaw = User::find($id);
        //dd($auRaw);
        return view('auth.edit',compact('auRaw'));
    }

    public function update(Request $request)
    {
        $msg = array();
        $messages = [  
            'name.required' => 'name es requerido... Obligatorio',
            'email.required' => 'email es requerido... Obligatorio',
            'password.required' => 'password es requerido... Obligatorio',
            'password-confirm.required' => 'password-confirm es requerido... Obligatorio' 
            ];
            //'identificacion' => 'required|unique:medida|max:20',
            $validator = Validator::make($request->all(), [ 
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
                'password-confirm' => 'required'

            ],$messages);  
            if ($validator->fails()) { 
                $failed = $validator->failed(); 
                //dd($failed);
                $msg[] = "Error Valide los datos ingresados";
                //dd($validator);
                return view('clientes.store')
                            ->withErrors($validator)
                            ->with("msg",$msg)
                            ->with("guardar",false)
                            ->with("failed",$failed);
            } 
    }

    public function delete($id)
    {
        $User = User::find($id);
        //dd($User);
        $UserRows = User::where('id', $id)->delete();
         
        return redirect('/usuarios');
    }
}