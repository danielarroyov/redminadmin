<div class="nav-scroller bg-body shadow-sm">
	<nav class="nav nav-underline" aria-label="Secondary navigation">
		<a class="nav-link active" aria-current="page" href="{{url('usuarios')}}">Menu Usuarios</a> 
		<a class="nav-link" href="{{url('usuarios/create')}}">Crear Usuarios</a>    
	</nav>
</div>