@extends('layouts.app')

@section('content')
<div class="container">
  	<div class="row"> 
  		@include('auth.browser_usuarios') 
		<table class="table  table-success table-hover table-striped" width="100">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">First</th>
					<th scope="col">Last</th>
					<th scope="col">Handle</th>
				</tr>
		  	</thead>
		  	<tbody>
		  		
		  			<?php $row=0;  ?>
		  			@foreach  ($auRaw as $key) 
		  		<tr>
		  				<?php $row++   ?>
		  				<td scope="row">{{$row}}</td>
						<th scope="row">{{$key->name}}</th>
						<td scope="row">{{$key->email}}</td>
						
						<td scope="row" > 
	                        <a href="{{url('usuarios/edit/'.$key->id)}}" type="button" class="btn btn-secondary">EDITAR</a>
	                        <a href="{{url('usuarios/delete/'.$key->id)}}"type="button" class="btn btn-dark">ELIMINAR</a>
	                    </td>
				</tr>
                    @endforeach
		  	</tbody>
		</table>

	</div>
</section>
@endsection