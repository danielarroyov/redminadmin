<div class="nav-scroller bg-body shadow-sm">
	<nav class="nav nav-underline" aria-label="Secondary navigation">
		<a class="nav-link active" aria-current="page" href="{{url('clientes')}}">Menu Clientes</a> 
		<a class="nav-link" href="{{url('clientes/create')}}">Crear clientes</a> 
		<a class="nav-link" href="{{url('clientes/list')}}">Editar y Eliminar Clientes</a>  
	</nav>
</div>