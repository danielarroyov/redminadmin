@extends('layouts.app')

@section('content') 
<div class="container">
  	<div class="row"> 
		@include('clientes.browser_clientes') 
		<div class="my-3 p-3 bg-body rounded shadow-sm">
			
				@include('clientes.form_client')
		</div>
	</div>

	<div class="row">
		<div id="dialogo_mensajes"></div>
	</div>
</section>
<script type="text/javascript"> 
	jQuery(document).ready(function(e) {

	jQuery("#nit").val("{{$clientes->nit}}");

	jQuery("#first_name").val("{{$clientes->nombre}}");
	jQuery("#last_name").val("{{$clientes->apellido}}");
	jQuery("#direccion").val("{{$clientes->direccion}}");
	jQuery("#id").val("{{$clientes->Id}}"); 

       <?php if( trim( $clientes->cod_ciudad) != '' ){ ?>
            jQuery("#city" ).val("{{trim($clientes->cod_ciudad)}}");
       <?php } else{ ?>
            jQuery("#city" ).val('');
       <?php } ?>

       <?php if( trim( $clientes->cod_departamento) != '' ){ ?>
            jQuery("#department" ).val("{{trim($clientes->cod_departamento)}}");
       <?php } else{ ?>
            jQuery("#department" ).val('');
       <?php } ?>

		jQuery("#guardar").click(function(){ 
			datos = jQuery("#fx").serialize();  
			jQuery.ajax({
	            type:"POST"
	            ,url:"{{url('clientes/update/'.$clientes->Id)}}" 
	            ,async:false
	            ,data: datos
	            ,success: function(data){
	                jQuery("#dialogo_mensajes").html(data); 
	                //alert("ok");

	            }
	            ,error: function(data){
	            	alert("Error. EL NUMERO DE  CEDULA NO EXISTE "+data); 
	            } 
	        });
	    });
	});


</script>
@endsection