 
<form id="fx" name="fx" class="row g-3 needs-validation" >
	{{ method_field('POST') }}	
	@csrf
      <input type="hidden" id="id" name="id" value="">
   	<div class="col-md-4">
      	<label for="nit" class="form-label">cedula</label>
      	<input placeholder="daniel" type="text" class="form-control" id="nit" name="nit" value="" required>
      	<div class="valid-feedback"> 
      		Looks good!
      	</div>
   	</div>
   	<div class="col-md-4">
      	<label for="first_name" class="form-label">Primer nombre</label>
      	<input placeholder="daniel" type="text" class="form-control" id="first_name" name="first_name" value="" required>
      	<div class="valid-feedback"> 
      		Looks good!
      	</div>
   	</div>
   	<div class="col-md-4">
      	<label for="last_name" class="form-label">Primer Apellido</label>
      	<input placeholder="arroyo" type="text" class="form-control" id="last_name" name="last_name" value="" required>
      	<div class="valid-feedback">
         	Looks good!
      	</div>
   	</div>
   	<!--<div class="col-md-4">
      	<label for="CustomUsername" class="form-label">nombre de usuario </label>
      	<div class="input-group has-validation">
         	<span class="input-group-text" id="inputGroupPrepend">@</span>
         	<input placeholder="daniel.arroyo" type="text" class="form-control" id="CustomUsername" name="CustomUsername" aria-describedby="inputGroupPrepend" required>
         	<div class="invalid-feedback">
            	Please choose a username.
         	</div>
     	 </div>
   	</div>-->
   	<div class="col-md-6">
      	<label for="direccion" class="form-label">Direccion</label>
      	<input placeholder="Calle x" type="text" class="form-control" id="direccion" name="direccion" required>
      	<div class="invalid-feedback">
         	Please provide a valid city.
      	</div>
   	</div>
   	<div class="col-md-3">
      	<label for="city" class="form-label">Ciudad</label>
      	<select class="form-select" id="city" name="city" required>
         	<option selected disabled value="">Selecione...</option>
         	<option value="20001">Monteria</option>
      	</select>
      	<div class="invalid-feedback">
         	Please select a valid state.
      	</div>
   	</div>
   	<div class="col-md-3">
      	<label for="department" class="form-label">departamento</label>
      	<select class="form-select" id="department" name="department" required>
         	<option selected disabled value="">Selecione...</option>
         	<option value="20">Córdoba</option>
      	</select>
      	<div class="invalid-feedback">
         	Please select a valid state.
      	</div>
   	</div> 
   	<div class="col-12">
      	<button class="btn btn-primary" type="button" id="guardar">Submit form</button>
   	</div> 
</form>