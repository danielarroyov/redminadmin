
@if ($errors->any())
	@if (count($errors) > 0)

           @foreach ($errors->all() as $error)
               <div class="alert alert-danger">
			       <ul>
			       		<li>{{ $error }}</li> 
			       </ul>
			   </div>  
			   <script type="text/javascript">
			        jQuery(document).ready(function(){
			                $.smallBox({
							title : "Alerta",
							content : "<i class='fa fa-clock-o'></i> <i>{{ $error }}</i>",
							color : "#C46A69",
							iconSmall : "fa fa-warning shake animated",
							timeout : 6000,
							sound_file: "../../../../SmartAdmin/sound/smallbox"
							});
						});
				</script>
	  @endforeach
	@endif
@endif
@if(isset($failed))
 	<?php
		foreach (array_keys($failed) as $key ) {
			echo  "<script>jQuery('#".$key."').css('border-color','rgb(241, 75, 75)');</script>"."\n";
		}

 	 ?>
	
@endif

@if( isset($msg) )
	@if( count($msg) > 0 )
		@foreach ($msg as $mensaje)
			<div class="alert alert-info fade in">
		       	<ul>
		          	<li><i class="fa-fw fa fa-info"></i> <strong>Informacion: </strong> {{ $mensaje }}</li>
		       	</ul>
		   	</div> 
		   	<script type="text/javascript">
		        jQuery(document).ready(function(){
		                $.smallBox({
						title : "Alerta",
						content : "<i class='fa fa-clock-o'></i> <i>{{ $mensaje }}</i>",
						color : @if( $guardar==true )"#63b04f",@else  "#C46A69", @endif
						iconSmall : "fa fa-warning shake animated",
						timeout : 4000,
						sound_file: "../../../../SmartAdmin/sound/smallbox"
						});
					});
			</script>
		@endforeach
	@endif
@endif 
@if(isset($guardar) )
	@if( $guardar == 'true' ) 
			<script type="text/javascript">

				jQuery.bigBox({
					title : "Mensaje Satisfactorio",
					content :"Felicidades Actualizado Satisfactoriamente",   
					color : "#739E73",
					timeout: 9000,
					icon : "fa fa-check",
					number : "1",
					sound_file: "../../../../SmartAdmin/sound/smallbox"
					}, function() {
					closedthis();
				});  

			</script>
			@foreach ($msg as $mensaje)
			<div class="alert alert-info fade in">
		       	<ul>
		          	<li><i class="fa-fw fa fa-info"></i> <strong>Informacion: </strong> {{ $mensaje }}</li>
		       	</ul>
		   	</div> 
			@endforeach
	@endif
@endif

@if( $cli == 'true' ) 
	<script type="text/javascript">
		url="{{url('clientes/list')}}";
		jQuery(location).attr('href',url);
	</script> 
@endif
