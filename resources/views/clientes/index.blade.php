@extends('layouts.app')

@section('content')
<div class="container">
  	<div class="row"> 
		@include('clientes.browser_clientes')

			<div class="my-3 p-3 bg-body rounded shadow-sm">
				<h6 class="border-bottom pb-2 mb-0">listado de clientes</h6> 
		    	@if (! empty($clientes))
				    @foreach ($clientes as $fila ) 
					<div class="d-flex text-muted pt-3">

						<img src="https://github.com/mdo.png" alt=""  class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32"   role="img" aria-label="Placeholder: 32x32" focusable="false"> 
						<p class="pb-3 mb-0 small lh-sm border-bottom">
							<strong class="d-block text-gray-dark">{{$fila->nombre}} {{$fila->apellido}}</strong>
							{{$fila->direccion}}
						</p>
		    		</div>
		    	
				    @endforeach
				@endif  
		    <small class="d-block text-end mt-3">
		      	<a href="#">mas</a>
		    </small>
		  </div>

		</div>   
	</div>
</section>

@endsection