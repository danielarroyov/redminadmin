@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row"> 
      @include('clientes.browser_clientes')
         <div class="my-3 p-3 bg-body rounded shadow-sm">
            <h6 class="border-bottom pb-2 mb-0">Suggestions</h6>
            @if (! empty($clientes))
                @foreach ($clientes as $fila )  
            <div class="d-flex text-muted pt-3">
               <svg class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32" preserveAspectRatio="xMidYMid slice" focusable="false">
                  <title>Placeholder</title>
                  <rect width="100%" height="100%" fill="#007bff"></rect>
                  <text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text>
               </svg>
               <div class="pb-3 mb-0 small lh-sm border-bottom w-100">
                  <div class="d-flex justify-content-between">
                     <strong class="text-gray-dark">{{$fila->nombre}} {{$fila->apellido}}</strong>
                     <div class=" bd-example ">
                        <a href="{{url('clientes/edit/'.$fila->Id)}}" type="button" class="btn btn-secondary">EDITAR</a>
                        <a href="{{url('clientes/delete/'.$fila->Id)}}"type="button" class="btn btn-dark">ELIMINAR</a>
                     </div>
                  </div>
                  <span class="d-block">{{$fila->direccion}}</span>
               </div>
            </div> 

               
               
               @endforeach
            @endif
            <small class="d-block text-end mt-3">
            <a href="#">All suggestions</a>
            </small>
         </div>

   </div>
</section>

<script type="text/javascript">
   
</script>

@endsection