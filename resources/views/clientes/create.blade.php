@extends('layouts.app')

@section('content') 
<div class="container">
  	<div class="row"> 
		@include('clientes.browser_clientes') 
		<div class="my-3 p-3 bg-body rounded shadow-sm">
			
				@include('clientes.form_client')
		</div>
	</div>

	<div class="row">
		<div id="dialogo_mensajes"></div>
	</div>
</section>
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		jQuery("#guardar").click(function(){ 
			datos = jQuery("#fx").serialize();  
			jQuery.ajax({
	            type:"POST"
	            ,url:"{{ url('clientes/store') }}"
	            ,async:false
	            ,data: datos
	            ,success: function(data){
	                jQuery("#dialogo_mensajes").html(data); 
	                //alert("ok");

	            }
	            ,error: function(data){
	            	alert("Error. EL NUMERO DE  CEDULA NO EXISTE "+data);
	                //jQuery("#dialogo_mensajes").html("Error Cargando este Elemento" + data);
	                //jQuery("#dialogo_mensajes").dialog("open");
	                jQuery("#wrapper").css('display', 'none');
	                jQuery('#wrapper').hide();
	                //window.location.reload();
	            } 
	        });
	    });
	});


</script>
@endsection